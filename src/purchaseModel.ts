import * as moment from 'moment';

/** 購入モデル */
export interface PurchaseModel {
    /** 購入ID */
    id: string;
    /** 商品名 */
    purchaseName: string;
    /** 商品価格 */
    purchasePrice: number;
    /** 氏名（名） */
    userFirstName: string;
    /** 氏名（姓） */
    userFamilyName: string;
    /** 購入日 */
    purchaseDate: moment.Moment;
}

/**
 * 表示用の購入モデル
 */
export interface PurchaseDispModel extends PurchaseModel {
    /** 名前（表示用） */
    userNameForDisp?: string;
    /** 商品価格(表示用) */
    purchasePriceForDisp?: string;
    /** 購入日（表示用） */
    purchaseDateForDisp?: string;
}