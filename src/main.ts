import { PurchaseDispModel, PurchaseModel } from "./purchaseModel";
import { DispUtil } from "./util";
import { TestDataCreator } from "./testData";

/** 購入履歴を生成 */
const purchaseHistory: PurchaseModel[] = TestDataCreator.createPurchaseHistory();
// 問題1実行
question1(purchaseHistory);
// 問題2実行
question2(purchaseHistory);
// 問題3実行
question3(purchaseHistory);
// 問題4実行
question4(purchaseHistory);

/**
 * 【問題1】購入履歴を全て表示してください。
 *  ただし、下記の例の通りフォーマットを変換し、表示用のModel(PurchaseDispModel)に変換して表示してください。
 *  名前：氏名（姓） + " " + 氏名（名）
 *  商品価格:3,000円
 *  購入日:2021年10月02日(※)
 *  ※：10月02日は、松鷹の誕生日です（笑）
 * 【使用する技術】
 *  ・配列.map(forEachは使用禁止です)
 * @param purchaseHistoryList 商品購入リスト
 */
function question1(purchaseHistoryList: PurchaseModel[]): void {
    console.log("■ 問題1 結果 ==========");
    // 【ヒント】配列.mapってどんな時に使う？
    //  配列.mapは、元の配列をベースに情報を再編成したい時に使用します。
    // 【その他】
    //  ・各種変換および表示はDispUtilを使用してください。
    const resultList = purchaseHistoryList.map(item => {
        const purchaseDisp: PurchaseDispModel = {
            // 購入ID
            id: item.id,
            // 商品名
            purchaseName: item.purchaseName,
            // 商品価格
            purchasePrice: item.purchasePrice,
            // 商品価格（表示用）
            purchasePriceForDisp: DispUtil.formatDispPrice(item.purchasePrice),
            // 氏名（名）
            userFirstName: item.userFirstName,
            // 氏名（姓）
            userFamilyName: item.userFamilyName,
            // 氏名（表示用）
            userNameForDisp: DispUtil.formatDispName(item.userFirstName, item.userFamilyName),
            // 購入日
            purchaseDate: item.purchaseDate,
            // 購入日（表示用）
            purchaseDateForDisp: DispUtil.formatDispDate(item.purchaseDate)
        }
        return purchaseDisp;
    });
    DispUtil.showDispList(resultList);
}

/**
 * 【問題2】購入した商品名の一覧を表示してください。
 * 　ただし、重複する商品名は削除してください。
 * 【使用する技術】
 *  ・配列.filter(forEachは使用禁止です)
 * @param purchaseHistoryList 表品購入リスト
 */
function question2(purchaseHistoryList: PurchaseModel[]): void {
    console.log("■ 問題2 結果 ==========");
    // 【ヒント】重複ってどうやって削除するの？
    //  配列.filterを使用します。
    //  っと、配列.filterのコールバック関数の第二引数には行番号、第三引数には元の配列が渡ってくる。
    //  ぷらす、配列のindexOfも使用するとできるんだがぁ～
    //  ↑で分かったら天才！！分からんかったらググってみて（笑）
    // 【その他】
    //  ・出力はリストをコンソール出力する感じでOKです
    //  [ 'ギター', '職業用ミシン', 'チャオチュール 50本', 'Google Pixcel 6' ]
    const resultList = purchaseHistoryList.map(item => item.purchaseName).filter((item, index, self) => {
        return self.indexOf(item) === index;
    });
    console.log(resultList);
}

/**
 * 【問題3】購入した商品の中で最も高額な商品情報（商品名と価格）を表示してください。
 *  ※：最高額の商品が複数件ある場合は考慮不要とします。
 * 【使用する技術】
 *  ・配列.reduce(forEachは使用禁止です)
 * @param purchaseHistoryList 表品購入リスト
 */
function question3(purchaseHistoryList: PurchaseModel[]): void {
    console.log("■ 問題3 結果 ==========");
    // 【ヒント】配列.reduceっていつ使うの？
    //  配列の各要素単体では分からず、横断的に見ていく必要がある場合に使用します。
    const result = purchaseHistoryList.reduce((item1, item2) => {
        return item1.purchasePrice > item2.purchasePrice ? item1 : item2;
    });
    console.log("商品名：" + result.purchaseName);
    console.log("商品価格：" + DispUtil.formatDispPrice(result.purchasePrice));
}

/**
 * 【問題4】月毎の購入金額を表示してください。
 * 【使用する技術】
 *  ・配列.reduce(forEachは使用禁止です)
 * @param purchaseHistoryList 表品購入リスト
 */
function question4(purchaseHistoryList: PurchaseModel[]): void {
    console.log("■ 問題4 結果 ==========");
    // 【ヒント】配列.reduceの第二引数を活用してください。
    const result = purchaseHistoryList.reduce((map, item) => {
        const key = item.purchaseDate.format("YYYYMM");
        const totalPrice = map.get(key) === undefined ? 0 : map.get(key) as number;
        return map.set(key, totalPrice + item.purchasePrice);
    }, new Map<string, number>());
    console.log(result);
}