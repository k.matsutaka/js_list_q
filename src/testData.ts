import { PurchaseModel } from "./purchaseModel";
import * as moment from "moment";

export class TestDataCreator {
    /**
     * 商品情報リストを生成する。
     * @returns 商品情報リスト
     */
    static createPurchaseHistory(): PurchaseModel[] {
        let retList: PurchaseModel[] = [];
        const data1: PurchaseModel = {
            id: "001",
            userFamilyName: "松鷹",
            userFirstName: "憲",
            purchaseName: "ギター",
            purchasePrice: 50000,
            purchaseDate: moment([2021, 10 - 1, 2])
        };
        retList.push(data1);
        const data2: PurchaseModel = {
            id: "002",
            userFamilyName: "松鷹",
            userFirstName: "憲",
            purchaseName: "職業用ミシン",
            purchasePrice: 60000,
            purchaseDate: moment([2021, 8 - 1, 30])
        };
        retList.push(data2);
        const data3: PurchaseModel = {
            id: "003",
            userFamilyName: "松鷹",
            userFirstName: "憲",
            purchaseName: "チャオチュール 50本",
            purchasePrice: 2000,
            purchaseDate: moment([2021, 8 - 1, 1])
        };
        const data4: PurchaseModel = {
            id: "004",
            userFamilyName: "松鷹",
            userFirstName: "憲",
            purchaseName: "チャオチュール 50本",
            purchasePrice: 2000,
            purchaseDate: moment([2021, 9 - 1, 1])
        };
        retList.push(data4);
        const data5: PurchaseModel = {
            id: "005",
            userFamilyName: "松鷹",
            userFirstName: "憲",
            purchaseName: "チャオチュール 50本",
            purchasePrice: 2000,
            purchaseDate: moment([2021, 10 - 1, 1])
        };
        retList.push(data5);
        const data6: PurchaseModel = {
            id: "006",
            userFamilyName: "松鷹",
            userFirstName: "憲",
            purchaseName: "Google Pixcel 6",
            purchasePrice: 70000,
            purchaseDate: moment([2021, 10 - 1, 26])
        };
        retList.push(data6);
        return retList;
    }
}