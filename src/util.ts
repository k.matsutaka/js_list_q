import { PurchaseDispModel } from "./purchaseModel";

/** 表示用のユーティリティクラス */
export class DispUtil {
    /**
     * YYYY年MM月DD日の形式にフォーマット
     * @param date 日付
     * @returns YYYY年MM月DD日の形式の日付
     */
    static formatDispDate(date: moment.Moment): string {
        return date.year() + "年" + (date.month() + 1) + "月" + date.date() + "日"
    }

    /**
     * 表示用の価格に変換
     * @param price 価格
     * @returns 表示用の価格
     */
    static formatDispPrice(price: number): string {
        return String(price).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,') + "円";
    }

    /**
     * 表示用の名前に変換
     * @param firstName 名前（名）
     * @param familyName 名前（姓）
     * @returns 表示用の名前
     */
    static formatDispName(firstName: string, familyName: string): string {
        return familyName + " " + firstName;
    }

    /**
     * 表示用の関数
     * @param dispModelList 表示用関数リスト
     */
    static showDispList(dispModelList: PurchaseDispModel[]) {
        dispModelList.forEach((item, index) => {
            console.log("〇" + (index + 1) + "行目 ----------");
            // 購入ID
            console.log("購入ID：" + item.id);
            // 商品名
            console.log("商品名：" + item.purchaseName);
            // 商品価格（表示用）
            console.log("商品価格：" + item.purchasePriceForDisp);
            // 氏名（表示用）
            console.log("氏名：" + item.userNameForDisp);
            // 購入日（表示用）
            console.log("購入日：" + item.purchaseDateForDisp)
            if (index === dispModelList.length - 1) {
                console.log("---------------");
            }
        });
    }
}